#include "HelheimBoard.h"
#include "HelheimGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerControllers/BoardPlayerController.h"
#include "Gamemodes/BoardGameMode.h"
#include "Pawns/BoardPawn.h"
#include "GameModes/MinigameGameMode.h"
#include "Actors/Board/Tiles/PremiumCurrencyMerchantTile.h"

int UHelheimBoard::maxRounds = 1;
int UHelheimBoard::maxPremiumCurrency = 1;

UHelheimBoard::UHelheimBoard(UHelheimGameInstance* gi)
{
	gameInstance = gi;

	for (auto PlayerIt = gi->GetLocalPlayerIterator(); PlayerIt; ++PlayerIt)
	{
		const ULocalPlayer* localPlayer = *PlayerIt;
		playersBoardData.Add(localPlayer->GetControllerId());
	}

	currentPlayer = gi->GetFirstGamePlayer()->GetControllerId();
}

const FPlayerBoardData* UHelheimBoard::GetPlayerBoardData(ControllerId controllerId) const
{
	if (const FPlayerBoardData* playerBoardData = playersBoardData.Find(controllerId))
	{
		return playerBoardData;
	}

	checkf(false, TEXT("Controller id not found in map."));

	return nullptr;
}

void UHelheimBoard::SetPlayerEndPlaceInMinigame(ControllerId controllerId, int place)
{
	if (FPlayerBoardData* playerBoardData = playersBoardData.Find(controllerId))
	{
		playerBoardData->endPlaceInMinigame = place;
	}
	else
	{
		checkf(false, TEXT("Controller id not found in map."));
	}
}

AHelheimPlayerController* UHelheimBoard::GetCurrentPlayer()
{
	for (auto PlayerIt = gameInstance->GetLocalPlayerIterator(); PlayerIt; ++PlayerIt)
	{
		const ULocalPlayer* localPlayer = *PlayerIt;

		if(localPlayer->GetControllerId() == currentPlayer)
		{
			return Cast<AHelheimPlayerController>(localPlayer->GetPlayerController(localPlayer->GetWorld()));
		}
	}

	return nullptr;
}

void UHelheimBoard::SetCurrentPlayer(APlayerController* player)
{
	SetCurrentPlayer(player->GetLocalPlayer()->GetControllerId());
}

void UHelheimBoard::SetCurrentPlayer(ControllerId player)
{
	if (ABoardPlayerController* previousPlayerController = Cast<ABoardPlayerController>(GetCurrentPlayer()))
	{
		previousPlayerController->SetIsCurrentPlayer(false);
	};

	currentPlayer = player;

	if (ABoardPlayerController* currentPlayerController = Cast<ABoardPlayerController>(GetCurrentPlayer()))
	{
		currentPlayerController->SetIsCurrentPlayer(true);
	};
}

void UHelheimBoard::CacheCurrentPlayersBoardData()
{
	const ABoardGameMode* boardGameMode = gameInstance->GetWorld()->GetAuthGameMode<ABoardGameMode>();
	const ABoardPawn* playerPawn = GetCurrentPlayer()->GetPawn<ABoardPawn>();
	AHelheimBoardTile* currentTile = playerPawn->GetCurrentTileInfo().tile;
	playersBoardData[currentPlayer].tileIndex = boardGameMode->GetTileIndex(currentTile);
}

void UHelheimBoard::CacheAllPlayersMinigameData()
{
	for (const TPair<ControllerId, FPlayerBoardData>& pair : playersBoardData)
	{
		if (APlayerController* playerController = UGameplayStatics::GetPlayerControllerFromID(gameInstance, pair.Key))
		{
			const int endPlace = gameInstance->GetWorld()->GetAuthGameMode<AMinigameGameMode>()->GetPlayerEndPlace(playerController);
			playersBoardData[pair.Key].endPlaceInMinigame = endPlace;
		}
	}
}

void UHelheimBoard::SwitchToNextPlayer()
{
	CacheCurrentPlayersBoardData();

	const TArray<ULocalPlayer*> localPlayers = gameInstance->GetLocalPlayers();	
	int foundIndex = 0;
	for(; foundIndex < localPlayers.Num(); foundIndex++)
	{
		if(localPlayers[foundIndex]->GetControllerId() == currentPlayer)
		{
			break;
		}
	}

	//KS - Transitioning into end of round mini-game.
	if(foundIndex == localPlayers.Num() - 1)
	{
		SetCurrentPlayer(localPlayers[0]->GetControllerId());
		SetBoardState(EBoardState::TransitionToMinigame);
		gameInstance->GetWorld()->GetAuthGameMode<ABoardGameMode>()->StartRandomMinigame();
	}
	else 
	{
		SetCurrentPlayer(localPlayers[foundIndex + 1]->GetControllerId());
	}
}

void UHelheimBoard::OnGamemodeBegin()
{
	SetCurrentPlayer(currentPlayer);
	HandleStatePostTransition();
}

void UHelheimBoard::SetBoardState(EBoardState state)
{
	boardState = state;
	HandleStatePreTransition();
}

void UHelheimBoard::TeleportCharactersToCachedPosition()
{
	for (const TPair<ControllerId, FPlayerBoardData>& pair : playersBoardData)
	{
		if(APlayerController* playerController = UGameplayStatics::GetPlayerControllerFromID(gameInstance, pair.Key))
		{
			AHelheimBoardTile* tile = gameInstance->GetWorld()->GetAuthGameMode<ABoardGameMode>()->GetTile(pair.Value.tileIndex);
			playerController->GetPawn<ABoardPawn>()->MoveToTile(tile, true);
		}
	}
}

void UHelheimBoard::ChooseRandomPremiumCurrencyMerchantTile()
{
	const ABoardGameMode* gamemode = gameInstance->GetWorld()->GetAuthGameMode<ABoardGameMode>();
	TArray<APremiumCurrencyMerchantTile*> merchantTiles = gamemode->premiumCurrencyMerchantTiles;

	if (merchantTiles.Num() > 1 && premiumCurrencyMerchantTileIndex != -1)
	{
		merchantTiles.Remove(Cast<APremiumCurrencyMerchantTile>(gamemode->GetTile(premiumCurrencyMerchantTileIndex)));
	}

	premiumCurrencyMerchantTileIndex = gamemode->GetTileIndex(merchantTiles[FMath::RandRange(0, merchantTiles.Num() - 1)]);

	UpdatePremiumCurrencyMerchantTile();
}

void UHelheimBoard::UpdatePremiumCurrencyMerchantTile()
{
	APremiumCurrencyMerchantTile* tile = Cast<APremiumCurrencyMerchantTile>(gameInstance->GetWorld()->GetAuthGameMode<ABoardGameMode>()->GetTile(premiumCurrencyMerchantTileIndex));
	tile->SetMerchantActive(true);
}

void UHelheimBoard::ApplyMinigameRewards()
{
	for (const TPair<ControllerId, FPlayerBoardData>& pair : playersBoardData)
	{
		const int amountToAdd = GetPositionCurrencyGainedViaMinigame(playersBoardData[pair.Key].endPlaceInMinigame);
		AddPlayerSouls(pair.Key, amountToAdd);
	}
}

void UHelheimBoard::HandleStatePreTransition()
{
	switch (boardState)
	{
	case EBoardState::Uninitialised:
		break;
	case EBoardState::TransitionFromMinigame:
		CacheAllPlayersMinigameData();
		break;
	case EBoardState::TransitionToMinigame:
		break;
	}
}

void UHelheimBoard::HandleStatePostTransition()
{
	switch(boardState)
	{
	case EBoardState::Uninitialised:
		TeleportCharactersToCachedPosition();
		ChooseRandomPremiumCurrencyMerchantTile();
		break;
	case EBoardState::TransitionFromMinigame:
		TeleportCharactersToCachedPosition();
		UpdatePremiumCurrencyMerchantTile();
		ApplyMinigameRewards();
		if (++currentRound > maxRounds)
		{
			TriggerResultScreen();
		}
		break;
	case EBoardState::TransitionToMinigame:
		break;
	}
}

int UHelheimBoard::GetPlayerSoulsCount(ControllerId controllerId) const
{
	if (const FPlayerBoardData* playerBoardData = playersBoardData.Find(controllerId))
	{
		return playerBoardData->soulsCount;
	}

	checkf(false, TEXT("Controller id not found in map."));
	return -1;
}

int UHelheimBoard::GetPlayerPremiumCurrencyCount(ControllerId controllerId) const
{
	if (const FPlayerBoardData* playerBoardData = playersBoardData.Find(controllerId))
	{
		return playerBoardData->premiumCurrencyCount;
	}

	checkf(false, TEXT("Controller id not found in map."));
	return -1;
}

int UHelheimBoard::GetPositionCurrencyGainedViaMinigame(int position) const
{
	return soulsBaseRewardRate / (position + 1);
}

void UHelheimBoard::AddPlayerSouls(ControllerId controllerId, int amount)
{
	if (FPlayerBoardData* playerBoardData = playersBoardData.Find(controllerId))
	{
		if (APlayerController* playerController = UGameplayStatics::GetPlayerControllerFromID(gameInstance, controllerId))
		{
			playerBoardData->soulsCount += amount;

			if (playerBoardData->soulsCount < 0)
			{
				playerBoardData->soulsCount = 0;
			}

			playerController->GetPawn<ABoardPawn>()->OnSoulsCountChanged.Broadcast(playerBoardData->soulsCount);
		}
	}
	else
	{
		checkf(false, TEXT("Controller id not found in map."));
	}
}

void UHelheimBoard::AddPlayerPremiumCurrency(ControllerId controllerId, int amount)
{
	if (FPlayerBoardData* playerBoardData = playersBoardData.Find(controllerId))
	{
		if (APlayerController* playerController = UGameplayStatics::GetPlayerControllerFromID(gameInstance, controllerId))
		{
			playerBoardData->premiumCurrencyCount += amount;

			if (playerBoardData->premiumCurrencyCount < 0)
			{
				playerBoardData->premiumCurrencyCount = 0;
			}

			playerController->GetPawn<ABoardPawn>()->OnPremiumCurrencyCountChanged.Broadcast(playerBoardData->premiumCurrencyCount);

			if(playerBoardData->premiumCurrencyCount == maxPremiumCurrency)
			{
				TriggerResultScreen();
			}
		}
	}
	else
	{
		checkf(false, TEXT("Controller id not found in map."));
	}
}

int UHelheimBoard::GetControllerIdLeaderboardPosition(ControllerId controllerId)
{
	int position = 0;

	for (const TPair<ControllerId, FPlayerBoardData>& pair : playersBoardData)
	{
		if (pair.Key != controllerId)
		{
			const int sortPremiumCurrencyCount = pair.Value.premiumCurrencyCount;
			const int sortStandardCurrencyCount = pair.Value.soulsCount;
			const int currentPremiumCurrencyCount = playersBoardData[controllerId].premiumCurrencyCount;
			const int currentStandardCurrencyCount = playersBoardData[controllerId].soulsCount;

			const bool lessPremiumCurrency = currentPremiumCurrencyCount < sortPremiumCurrencyCount;
			const bool lessStandardCurrency = currentStandardCurrencyCount < sortStandardCurrencyCount;

			if(lessPremiumCurrency || (currentPremiumCurrencyCount == sortPremiumCurrencyCount && lessStandardCurrency))
			{
				position += 1;
			}
		}
	}

	return position;
}

void UHelheimBoard::TriggerResultScreen()
{
	UGameplayStatics::OpenLevel(gameInstance, TEXT("BoardResult"));
}