#include "HelheimGameModeBase.h"
#include "HelheimPlayerState.h"
#include "HelheimPlayerController.h"
#include "Engine/LevelStreaming.h"

AHelheimGameModeBase::AHelheimGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerStateClass = AHelheimPlayerState::StaticClass();
	PlayerControllerClass = AHelheimPlayerController::StaticClass();
}

FName AHelheimGameModeBase::GetRandomLevelStreamingName() const
{
	const TArray<ULevelStreaming*> streamingLevels = GetWorld()->GetStreamingLevels();
	return streamingLevels[FMath::RandRange(0, streamingLevels.Num() - 1)]->GetWorldAssetPackageFName();
}