#include "HelheimGameInstance.h"
#include "HelheimPlayerController.h"
#include "HelheimLocalPlayer.h"

UHelheimBoard* UHelheimGameInstance::SetupBoard()
{
	checkf(!board, TEXT("Board already initialised!"));
	board = MakeShared<UHelheimBoard>(this);
	return board.Get();
}

void UHelheimGameInstance::Shutdown()
{
	ResetBoard();
}

void UHelheimGameInstance::GetPlayerControllers(TArray<AHelheimPlayerController*>& outArray) const
{
	for (auto PlayerIt = GetWorld()->GetPlayerControllerIterator(); PlayerIt; ++PlayerIt)
	{
		AHelheimPlayerController* playerController = Cast<AHelheimPlayerController>(*PlayerIt);
		outArray.Add(playerController);
	}
}