#include "LivesCharacter.h"
#include "GameModes/LivesMinigameGameMode.h"

void ALivesCharacter::DeductLives(int deductAmount) 
{ 
	if((livesRemaining -= deductAmount) <= 0)
	{
		APlayerController* playerController = GetController<APlayerController>();
		playerController->SetPawn(nullptr);
		GetWorld()->GetAuthGameMode<ALivesMinigameGameMode>()->OnCharacterDeath(playerController);
		
		GetMesh()->SetSimulatePhysics(true);
		GetMesh()->SetCollisionResponseToAllChannels(ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	}
};