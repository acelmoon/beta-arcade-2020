#pragma once

#include "CoreMinimal.h"
#include "HelheimCharacter.h"
#include "LivesCharacter.generated.h"

UCLASS()
class BETAARCADE_API ALivesCharacter : public AHelheimCharacter
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void DeductLives(int deductAmount);
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int livesRemaining = 1;

};
