#include "HelheimPawn.h"
#include "HelheimPlayerController.h"

AHelheimPawn::AHelheimPawn() : Super()
{
	PrimaryActorTick.bCanEverTick = false;

	USceneComponent* actorRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = actorRootComponent;

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalMesh(TEXT("SkeletalMesh'/Game/ArtAssets/Mesh/SKM_Character.SKM_Character'"));
	static ConstructorHelpers::FObjectFinder<UAnimBlueprintGeneratedClass> AnimationBlueprint(TEXT("AnimBlueprint'/Game/Animations/ABP_Pawn.ABP_Pawn_C'"));

	Mesh = CreateOptionalDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	if (Mesh)
	{
		Mesh->bOwnerNoSee = false;
		Mesh->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPose;
		Mesh->bCastDynamicShadow = true;
		Mesh->bAffectDynamicIndirectLighting = true;
		Mesh->PrimaryComponentTick.TickGroup = TG_PrePhysics;
		Mesh->SetupAttachment(RootComponent);
		Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		Mesh->SetCollisionProfileName(TEXT("OverlapAll"));
		Mesh->SetGenerateOverlapEvents(false);
		Mesh->SetCanEverAffectNavigation(false);
		Mesh->SetSkeletalMesh(SkeletalMesh.Object);
		Mesh->SetRelativeLocation(FVector(0, 0, -75));
		Mesh->AnimClass = AnimationBlueprint.Object;
	}
}

void AHelheimPawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	Mesh->SetVectorParameterValueOnMaterials(TEXT("PlayerColour"), FVector(GetController<AHelheimPlayerController>()->GetPlayerColour()));
}

