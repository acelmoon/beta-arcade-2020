
#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Utilities/Typedefs.h"
#include "HelheimBoard.h"
#include "HelheimGameInstance.generated.h"

class AHelheimPlayerController;

USTRUCT()
struct FPlayerData
{
public:
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	FColor color;
};


UCLASS(BlueprintType)
class BETAARCADE_API UHelheimGameInstance : public UGameInstance
{
	GENERATED_BODY()

	TSharedPtr<UHelheimBoard> board = nullptr;
	TMap<ControllerId, FPlayerData*> playerData;

public:
	UPROPERTY(EditDefaultsOnly)
	FLinearColor playerColours[4]
	{
		FLinearColor(0.06, 0.52, 0.06),
		FLinearColor(0.45, 0.74, 0.28),
		FLinearColor(0.29, 0.86, 0.93),
		FLinearColor(0.47, 0.27, 0.87)
	};

	UPROPERTY(EditDefaultsOnly)
	FLinearColor playerSecondaryColours[4]
	{
		FLinearColor(0.06, 0.52, 0.06),
		FLinearColor(0.45, 0.74, 0.28),
		FLinearColor(0.29, 0.86, 0.93),
		FLinearColor(0.47, 0.27, 0.87)
	};

public:
	UHelheimBoard* SetupBoard();
	UHelheimBoard* GetBoard() const { return board.Get(); };
	FPlayerData* GetPlayerData(ControllerId controllerId) const { return playerData[controllerId]; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	void GetPlayerControllers(TArray<AHelheimPlayerController*>& outArray) const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FLinearColor GetPlayerColour(int index) const { return playerColours[index]; };

	void ResetBoard() { board.Reset(); };

public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TArray<FName> availableMinigameMaps;

protected:
	void Shutdown() override;
};