#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HelheimCharacter.generated.h"

UCLASS()
class BETAARCADE_API AHelheimCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AHelheimCharacter();

	UFUNCTION(BlueprintCallable)
	void SetIsPoweredUp(bool poweredUp) { isPoweredUp = poweredUp; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsPoweredUp() const { return isPoweredUp; };
	
protected:
	void PossessedBy(AController* NewController) override;
	void MoveForward(float value);
	void MoveRight(float value);

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	bool isPoweredUp = false;
};
