#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HelheimPowerUp.generated.h"

UCLASS()
class BETAARCADE_API AHelheimPowerUp : public AActor
{
	GENERATED_BODY()
	
public:	
	AHelheimPowerUp();

protected:
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float powerUpLength = 5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float powerUpLifetime = 5.0f;
};
