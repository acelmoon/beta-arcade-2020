#include "HelheimBlueprintFunctionLibrary.h"
#include "Pawns/BoardPawn.h"
#include "Widgets/HelheimPlayerUserWidget.h"
#include "HelheimGameInstance.h"
#include "Components/PanelWidget.h"

void UHelheimBlueprintFunctionLibrary::SetupPanelWidgetChildren(UPanelWidget* panelWidget)
{
	int Index = 0;
	for (auto PlayerIt = panelWidget->GetWorld()->GetPlayerControllerIterator(); PlayerIt; ++PlayerIt)
	{
		Cast<UHelheimPlayerUserWidget>(panelWidget->GetChildAt(Index++))->SetupPlayerWidget(PlayerIt->Get());
	}
}

void UHelheimBlueprintFunctionLibrary::GetPlayerControllers(const UObject* WorldContextObject, TArray<AHelheimPlayerController*>& outArray)
{
	WorldContextObject->GetWorld()->GetGameInstance<UHelheimGameInstance>()->GetPlayerControllers(outArray);
} 

AHelheimPlayerController* UHelheimBlueprintFunctionLibrary::GetCurrentTurnsPlayerController(const UObject* WorldContextObject)
{
	return WorldContextObject->GetWorld()->GetGameInstance<UHelheimGameInstance>()->GetBoard()->GetCurrentPlayer();
}

int UHelheimBlueprintFunctionLibrary::GetPlayerIndexOnTile(const UObject* WorldContextObject, const AHelheimBoardTile* tile, const ABoardPawn* pawnToAvoid)
{
	int indexMask = 0;
	for (auto PlayerIt = WorldContextObject->GetWorld()->GetPlayerControllerIterator(); PlayerIt; ++PlayerIt)
	{
		if (const APlayerController* pc = PlayerIt->Get())
		{
			const ABoardPawn* player = pc->GetPawn<ABoardPawn>();
			if (player && player != pawnToAvoid)
			{
				const FCurrentTileInfo playerTile = player->GetCurrentTileInfo();
				if (playerTile.tile == tile)
				{
					indexMask += 1 << playerTile.tilePosition;
				}
			}
		}
	}

	int counter = 0;
	while(indexMask & 1)
	{
		indexMask = indexMask >> 1;
		counter++;
	}

	return counter;
}

int UHelheimBlueprintFunctionLibrary::GetPlayerLeaderboardPosition(const UObject* WorldContextObject, const APlayerController* playerController)
{
	return WorldContextObject->GetWorld()->GetGameInstance<UHelheimGameInstance>()->GetBoard()->GetControllerIdLeaderboardPosition(playerController->GetLocalPlayer()->GetControllerId());
}
