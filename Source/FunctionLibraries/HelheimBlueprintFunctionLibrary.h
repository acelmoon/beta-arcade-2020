#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "HelheimBlueprintFunctionLibrary.generated.h"

class AHelheimPlayerController;
class AHelheimBoardTile;

UCLASS()
class UHelheimBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "UI")
	static void SetupPanelWidgetChildren(UPanelWidget* panelWidget);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (WorldContext = "WorldContextObject"))
	static void GetPlayerControllers(const UObject* WorldContextObject, TArray<AHelheimPlayerController*>& outArray);

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (WorldContext = "WorldContextObject"))
	static AHelheimPlayerController* GetCurrentTurnsPlayerController(const UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static int GetPlayerIndexOnTile(const UObject* WorldContextObject, const AHelheimBoardTile* tile, const ABoardPawn* pawnToAvoid = nullptr);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static int GetPlayerLeaderboardPosition(const UObject* WorldContextObject, const APlayerController* playerController);
};
