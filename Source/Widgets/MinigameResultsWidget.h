#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Delegates/DelegateCombinations.h"
#include "MinigameResultsWidget.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReadyUp);

UCLASS()
class BETAARCADE_API UMinigameResultsWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnReadyUp OnReadyUp;

	UFUNCTION(BlueprintCallable)
	bool WasStartedByBoard() const;

	UFUNCTION(BlueprintCallable)
	void GetPlayerRewards(APlayerController* player, int& currencyAmount, int& positionInMinigame);
};
