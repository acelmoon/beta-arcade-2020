#include "MinigameResultsWidget.h"
#include "HelheimGameInstance.h"
#include "GameModes/MinigameGameMode.h"

bool UMinigameResultsWidget::WasStartedByBoard() const
{
	return GetGameInstance<UHelheimGameInstance>()->GetBoard() != nullptr;
}

void UMinigameResultsWidget::GetPlayerRewards(APlayerController* player, int& currencyAmount, int& positionInMinigame)
{
	positionInMinigame = GetWorld()->GetAuthGameMode<AMinigameGameMode>()->GetPlayerEndPlace(player);

	const UHelheimBoard* board = GetGameInstance<UHelheimGameInstance>()->GetBoard();
	currencyAmount = board ? board->GetPositionCurrencyGainedViaMinigame(positionInMinigame) : -1;
	
}