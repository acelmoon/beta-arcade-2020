#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HelheimPlayerUserWidget.generated.h"

UCLASS()
class BETAARCADE_API UHelheimPlayerUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetupPlayerWidget(APlayerController* playerController);
};
