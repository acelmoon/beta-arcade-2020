#include "HelheimPlayerController.h"
#include "HelheimLocalPlayer.h"

AHelheimPlayerController::AHelheimPlayerController() : Super()
{
	bShouldPerformFullTickWhenPaused = true;
}

FLinearColor AHelheimPlayerController::GetPlayerColour() const
{
	return Cast<UHelheimLocalPlayer>(GetLocalPlayer())->GetPlayerPrimaryColour();
}

FLinearColor AHelheimPlayerController::GetPlayerSecondaryColour() const
{
	return Cast<UHelheimLocalPlayer>(GetLocalPlayer())->GetPlayerSecondaryColour();
}