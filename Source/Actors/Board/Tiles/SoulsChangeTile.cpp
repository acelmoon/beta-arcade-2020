#include "SoulsChangeTile.h"
#include "Pawns/BoardPawn.h"
#include "HelheimGameInstance.h"
#include "GameFramework/PlayerController.h"
#include "NiagaraComponent.h"

ASoulsChangeTile::ASoulsChangeTile() : Super()
{
	ParticleSystem = CreateDefaultSubobject<UNiagaraComponent>(TEXT("ParticleSystem"));
	ParticleSystem->SetupAttachment(RootComponent);
	ParticleSystem->SetRelativeLocation(FVector(0.f, 0.f, 250.f));
	ParticleSystem->bAutoActivate = false;
}

void ASoulsChangeTile::OnTileLanded_Implementation(ABoardPawn* pawn)
{
	const ControllerId controllerId = pawn->GetController<APlayerController>()->GetLocalPlayer()->GetControllerId();
	GetGameInstance<UHelheimGameInstance>()->GetBoard()->AddPlayerSouls(controllerId, soulsAmountChange);
	ParticleSystem->ResetSystem();

	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, FTimerDelegate::CreateUObject(this, &ASoulsChangeTile::OnTileLandDelay, pawn), 6.0f, false);
}

void ASoulsChangeTile::OnTileLandDelay(ABoardPawn* pawn)
{
	Super::OnTileLanded_Implementation(pawn);
}

