#pragma once

#include "CoreMinimal.h"
#include "Actors/Board/HelheimBoardTile.h"
#include "PremiumCurrencyMerchantTile.generated.h"


UCLASS()
class BETAARCADE_API APremiumCurrencyMerchantTile : public AHelheimBoardTile
{
	GENERATED_BODY()
	
public:
	void OnTileAdded() override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetMerchantActive(bool isActive);

	UFUNCTION(BlueprintCallable)
	void OnPremiumCurrencyBought(class ABoardPawn* pawn);

	UFUNCTION(BlueprintCallable)
	APremiumCurrencyMerchantTile* GetActivePremiumCurrencyMerchantTile() const;

	UPROPERTY(BlueprintReadWrite)
	bool isActiveMerchant = false;

	UPROPERTY(EditDefaultsOnly)
	int premiumCurrencyObtainAmount = 1;
};
