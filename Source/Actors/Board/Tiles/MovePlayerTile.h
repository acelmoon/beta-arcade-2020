#pragma once

#include "CoreMinimal.h"
#include "Actors/Board/HelheimBoardTile.h"
#include "MovePlayerTile.generated.h"

UCLASS()
class BETAARCADE_API AMovePlayerTile : public AHelheimBoardTile
{
	GENERATED_BODY()

public:
	void OnTileLanded_Implementation(class ABoardPawn* pawn) override;

	UFUNCTION(BlueprintImplementableEvent)
	void HandlePawnMove(class ABoardPawn* pawn, FVector initialPosition);

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
	AHelheimBoardTile* tileToMoveTo;
};
