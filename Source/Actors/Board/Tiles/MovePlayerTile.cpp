#include "MovePlayerTile.h"
#include "Pawns/BoardPawn.h"
#include "HelheimGameInstance.h"
#include "GameFramework/PlayerController.h"

void AMovePlayerTile::OnTileLanded_Implementation(ABoardPawn* pawn)
{
	pawn->SetPawnTile(tileToMoveTo);
	HandlePawnMove(pawn, pawn->GetActorLocation());
}
