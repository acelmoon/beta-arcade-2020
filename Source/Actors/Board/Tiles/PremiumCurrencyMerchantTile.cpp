#include "PremiumCurrencyMerchantTile.h"
#include "HelheimPlayerController.h"
#include "HelheimGameInstance.h"
#include "Pawns/BoardPawn.h"
#include "Gamemodes/BoardGameMode.h"

void APremiumCurrencyMerchantTile::OnTileAdded()
{
	Super::OnTileAdded();
	GetWorld()->GetAuthGameMode<ABoardGameMode>()->premiumCurrencyMerchantTiles.Add(this);
}

void APremiumCurrencyMerchantTile::SetMerchantActive_Implementation(bool isActive)
{
	isActiveMerchant = isActive;
}

void APremiumCurrencyMerchantTile::OnPremiumCurrencyBought(ABoardPawn* pawn) 
{
	SetMerchantActive(false);
	const ControllerId controllerId = pawn->GetController<APlayerController>()->GetLocalPlayer()->GetControllerId();
	UHelheimBoard* board = GetGameInstance<UHelheimGameInstance>()->GetBoard();
	board->AddPlayerSouls(controllerId, -ABoardGameMode::GetPremiumCurrencyPrice());
	board->AddPlayerPremiumCurrency(controllerId, premiumCurrencyObtainAmount);
	board->ChooseRandomPremiumCurrencyMerchantTile();
}

APremiumCurrencyMerchantTile* APremiumCurrencyMerchantTile::GetActivePremiumCurrencyMerchantTile() const
{
	const UHelheimBoard* board = GetGameInstance<UHelheimGameInstance>()->GetBoard();
	const ABoardGameMode* gamemode = GetWorld()->GetAuthGameMode<ABoardGameMode>();
	return Cast<APremiumCurrencyMerchantTile>(gamemode->GetTile(board->GetPremiumCurrencyMerchantTileIndex()));
}