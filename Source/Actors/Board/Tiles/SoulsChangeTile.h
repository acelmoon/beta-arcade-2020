#pragma once

#include "CoreMinimal.h"
#include "Actors/Board/HelheimBoardTile.h"
#include "SoulsChangeTile.generated.h"

UCLASS()
class BETAARCADE_API ASoulsChangeTile : public AHelheimBoardTile
{
	GENERATED_BODY()

public:
	ASoulsChangeTile();

public:
	void OnTileLanded_Implementation(class ABoardPawn* pawn) override;

private:
	void OnTileLandDelay(class ABoardPawn* pawn);

private:
	UPROPERTY(EditDefaultsOnly)
	int soulsAmountChange = 30;

	UPROPERTY(EditDefaultsOnly)
	class UNiagaraComponent* ParticleSystem;
};
