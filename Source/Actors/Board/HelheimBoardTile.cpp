#include "HelheimBoardTile.h"
#include "GameModes/BoardGameMode.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Pawns/BoardPawn.h"
#include "HelheimGameInstance.h"

AHelheimBoardTile::AHelheimBoardTile()
{
	PrimaryActorTick.bCanEverTick = false;

	USceneComponent* actorRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = actorRootComponent;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> TileMesh(TEXT("StaticMesh'/Game/JackWArt/Meshes/SM_PlayerTile.SM_PlayerTile'"));
	TileStaticMeshComponent = CreateOptionalDefaultSubobject<UStaticMeshComponent>(TEXT("TileMesh"));
	if (TileStaticMeshComponent)
	{
		TileStaticMeshComponent->SetStaticMesh(TileMesh.Object);
		TileStaticMeshComponent->SetupAttachment(RootComponent);
		TileStaticMeshComponent->SetRelativeLocation(FVector(0.f, 0.f, -45.f));
	}
#if WITH_EDITORONLY_DATA
	if (UArrowComponent* arrowComponent = CreateEditorOnlyDefaultSubobject<UArrowComponent>(TEXT("Arrow")))
	{
		arrowComponent->SetupAttachment(RootComponent);
	}
#endif
}

void AHelheimBoardTile::BeginPlay()
{
	Super::BeginPlay();

	if (isStartTile)
	{
		GetWorld()->GetAuthGameMode<ABoardGameMode>()->SetupTiles(this);
	}
}

void AHelheimBoardTile::OnTileLanded_Implementation(ABoardPawn* pawn)
{
	GetGameInstance<UHelheimGameInstance>()->GetBoard()->SwitchToNextPlayer();
}

void AHelheimBoardTile::OnTilePassed_Implementation(ABoardPawn* pawn, AHelheimBoardTile* nextTile) 
{
	pawn->MoveToTile(nextTile);
}

void AHelheimBoardTile::DebugShowConnectedTiles()
{
	int i = 0;
	for (FConnectedTile connectedTile : branches)
	{
		FColor lineColour = UKismetMathLibrary::HSVToRGB(i * 10, 1, 1, 1).ToFColor(true);
		DrawDebugLine(GetWorld(), GetActorLocation(), connectedTile.tile->GetActorLocation(), lineColour, false, 1.0f, 0, 10.0f);

		if (!connectedTile.tile->isStartTile && !connectedTile.isReconnecting)
		{
			connectedTile.tile->DebugShowConnectedTiles();
		}

		i++;
	}
}

void AHelheimBoardTile::AutocorrectRotation()
{
	FVector averageLocation = FVector::ZeroVector;
	int divisionCounter = 0;
	for (FConnectedTile connectedTile : branches)
	{
		averageLocation = connectedTile.tile->GetActorLocation() + averageLocation;
		divisionCounter++;
	}

	SetActorRotation(FVector(averageLocation / divisionCounter - GetActorLocation()).Rotation());
}

void AHelheimBoardTile::AutocorrectRotationRecursive()
{
	AutocorrectRotation();

	for (FConnectedTile connectedTile : branches)
	{
		if (!connectedTile.tile->isStartTile && !connectedTile.isReconnecting)
		{
			connectedTile.tile->AutocorrectRotationRecursive();
		}
	}
}