#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "HelheimBoardTile.generated.h"

USTRUCT(BlueprintType)
struct FConnectedTile
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool isReconnecting = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AHelheimBoardTile* tile = nullptr;
};


UCLASS()
class BETAARCADE_API AHelheimBoardTile : public AActor
{
	GENERATED_BODY()
	
public:	
	AHelheimBoardTile();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
	TArray<FConnectedTile> branches;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnTileLanded(class ABoardPawn* pawn);
	virtual void OnTileLanded_Implementation(class ABoardPawn* pawn);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnTilePassed(class ABoardPawn* pawn, AHelheimBoardTile* nextTile);
	virtual void OnTilePassed_Implementation(class ABoardPawn* pawn, AHelheimBoardTile* nextTile);

	virtual void OnTileAdded() {};

	UPROPERTY(EditDefaultsOnly)
	class UStaticMeshComponent* TileStaticMeshComponent;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	bool isStartTile = false;

public:
	UFUNCTION(CallInEditor)
	void DebugShowConnectedTiles();

	UFUNCTION(CallInEditor)
	void AutocorrectRotation();

	UFUNCTION(CallInEditor)
	void AutocorrectRotationRecursive();

};
