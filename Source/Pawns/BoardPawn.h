#pragma once

#include "CoreMinimal.h"
#include "HelheimPawn.h"
#include "Actors/Board/HelheimBoardTile.h"
#include "BoardPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCurrencyCountChanged, int, newCurrencyCount);

USTRUCT(BlueprintType)
struct FCurrentTileInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
	AHelheimBoardTile* tile = nullptr;
	UPROPERTY(BlueprintReadOnly)
	int tilePosition = 0;
};

UCLASS()
class BETAARCADE_API ABoardPawn : public AHelheimPawn
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintPure, BlueprintCallable)
	FTransform GetTransformOnTile(const FCurrentTileInfo& tileInfo) const;

	UFUNCTION(BlueprintPure, BlueprintCallable)
	const FCurrentTileInfo& GetCurrentTileInfo() const { return currentTileInfo; };

	void SetPawnTile(AHelheimBoardTile* tile);

	UFUNCTION(BlueprintCallable)
	void MoveToTile(AHelheimBoardTile* tile, bool teleport = false);

	UFUNCTION(BlueprintCallable)
	void MoveAcrossTiles(int amountToMove, bool startOfMove, bool teleport = false);

	UFUNCTION(BlueprintNativeEvent)
	void SetIsCurrentPlayer(bool isCurrent);

	UFUNCTION(BlueprintCallable)
	int GetSoulsCount() const;

	UFUNCTION(BlueprintCallable)
	int GetPremiumCurrencyCount() const;

	UPROPERTY(BlueprintAssignable)
	FOnCurrencyCountChanged OnSoulsCountChanged;

	UPROPERTY(BlueprintAssignable)
	FOnCurrencyCountChanged OnPremiumCurrencyCountChanged;

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void StartMovementTimeline();

	UFUNCTION(BlueprintCallable)
	void FinishedMoving(bool teleport = false);

private:
	const float tileOffset = 50.0f;
	const float tileHeightOffset = 70.0f;

	int tilesToMove = 0;

	FCurrentTileInfo currentTileInfo;
};
