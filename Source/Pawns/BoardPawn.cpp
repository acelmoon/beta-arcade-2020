#include "BoardPawn.h"
#include "GameModes/BoardGameMode.h"
#include "FunctionLibraries/HelheimBlueprintFunctionLibrary.h"
#include "HelheimGameInstance.h"

FTransform ABoardPawn::GetTransformOnTile(const FCurrentTileInfo& tileInfo) const
{
	const FVector tileLocation = tileInfo.tile->GetActorLocation();
	const FVector tileForward = tileInfo.tile->GetActorForwardVector();
	const FVector tileUp = tileInfo.tile->GetActorUpVector();
	const FVector direction = tileForward * tileOffset;
	const FVector rotatedLocation = direction.RotateAngleAxis(tileInfo.tilePosition * 90 - 45, tileUp);

	return FTransform(tileInfo.tile->GetActorRotation(), rotatedLocation + tileLocation + tileUp * tileHeightOffset, GetActorScale3D());
}

void ABoardPawn::SetPawnTile(AHelheimBoardTile* tile)
{
	currentTileInfo.tile = tile;
	currentTileInfo.tilePosition = UHelheimBlueprintFunctionLibrary::GetPlayerIndexOnTile(this, tile, this);
}

void ABoardPawn::MoveToTile(AHelheimBoardTile* tile, bool teleport)
{
	SetPawnTile(tile);

	if (tilesToMove > 0)
	{
		tilesToMove--;
	}

	if (teleport)
	{
		SetActorTransform(GetTransformOnTile(currentTileInfo), false, false, ETeleportType::ResetPhysics);
		FinishedMoving(teleport);
	}
	else 
	{
		StartMovementTimeline();
	}
}

void ABoardPawn::MoveAcrossTiles(int amountToMove, bool startOfMove, bool teleport)
{
	tilesToMove = amountToMove;

	if (tilesToMove > 0) {
		AHelheimBoardTile* tileToMoveTo = nullptr;

		if (currentTileInfo.tile->branches.Num() > 1)
		{
			GetWorld()->GetAuthGameMode<ABoardGameMode>()->OpenBranchSelector(Cast<APlayerController>(GetOwner()));
			return;
		}
		else
		{
			tileToMoveTo = currentTileInfo.tile->branches[0].tile;
		}

		if(startOfMove)
		{
			MoveToTile(tileToMoveTo);
		}
		else if (!teleport)
		{
			currentTileInfo.tile->OnTilePassed(this, tileToMoveTo);
		}
	}
	else if(!teleport)
	{
		currentTileInfo.tile->OnTileLanded(this);
	}
}

void ABoardPawn::FinishedMoving(bool teleport)
{
	MoveAcrossTiles(tilesToMove, false, teleport);
}

void ABoardPawn::SetIsCurrentPlayer_Implementation(bool isCurrent)
{
	SetActorTickEnabled(isCurrent);

	if (isCurrent)
	{
		EnableInput(nullptr);
	}
	else
	{
		DisableInput(nullptr);
	}
}

int ABoardPawn::GetSoulsCount() const
{
	const ControllerId controllerId = GetController<APlayerController>()->GetLocalPlayer()->GetControllerId();
	return GetGameInstance<UHelheimGameInstance>()->GetBoard()->GetPlayerSoulsCount(controllerId);
}

int ABoardPawn::GetPremiumCurrencyCount() const
{
	const ControllerId controllerId = GetController<APlayerController>()->GetLocalPlayer()->GetControllerId();
	return GetGameInstance<UHelheimGameInstance>()->GetBoard()->GetPlayerPremiumCurrencyCount(controllerId);
}