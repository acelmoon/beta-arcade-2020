#include "HelheimLocalPlayer.h"
#include "HelheimGameInstance.h"

FLinearColor UHelheimLocalPlayer::GetPlayerPrimaryColour() const 
{
	return Cast<UHelheimGameInstance>(GetGameInstance())->playerColours[GetControllerId()];
}

FLinearColor UHelheimLocalPlayer::GetPlayerSecondaryColour() const
{
	return Cast<UHelheimGameInstance>(GetGameInstance())->playerSecondaryColours[GetControllerId()];
}