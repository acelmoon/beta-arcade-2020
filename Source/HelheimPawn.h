

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "HelheimPawn.generated.h"

UCLASS()
class BETAARCADE_API AHelheimPawn : public APawn
{
	GENERATED_BODY()

public:
	AHelheimPawn();

protected:
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh;

	void PossessedBy(AController* NewController) override;
};
