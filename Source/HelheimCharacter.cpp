#include "HelheimCharacter.h"
#include "Components/SphereComponent.h"
#include "HelheimPlayerController.h"
#include "Components/CapsuleComponent.h"
#include "Widgets/HelheimPlayerUserWidget.h"
#include "GameFramework/CharacterMovementComponent.h"

AHelheimCharacter::AHelheimCharacter() : Super()
{
	PrimaryActorTick.bCanEverTick = false;

	UCapsuleComponent* capsuleComponent = GetCapsuleComponent();
	capsuleComponent->InitCapsuleSize(29, 80);

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalMesh(TEXT("SkeletalMesh'/Game/ArtAssets/Mesh/SKM_Character.SKM_Character'"));
	GetMesh()->SetSkeletalMesh(SkeletalMesh.Object);
	GetMesh()->SetRelativeLocation(FVector(0, 0, -75));

	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	
	static ConstructorHelpers::FObjectFinder<UAnimBlueprintGeneratedClass> AnimationBlueprint(TEXT("AnimBlueprint'/Game/Animations/ABP_Character.ABP_Character_C'"));
	GetMesh()->AnimClass = AnimationBlueprint.Object;
	bUseControllerRotationYaw = false;
}

void AHelheimCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	AHelheimPlayerController* playerController = GetController<AHelheimPlayerController>();
	GetMesh()->SetVectorParameterValueOnMaterials(TEXT("PlayerColour"), FVector(playerController->GetPlayerColour()));
	GetMesh()->SetVectorParameterValueOnMaterials(TEXT("PlayerSecondaryColour"), FVector(playerController->GetPlayerSecondaryColour()));
}

void AHelheimCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AHelheimCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AHelheimCharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &AHelheimCharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Released, this, &AHelheimCharacter::StopJumping);
}

void AHelheimCharacter::MoveForward(float value)
{
	AddMovementInput(FVector(1, 0, 0), value);
}

void AHelheimCharacter::MoveRight(float value)
{
	AddMovementInput(FVector(0, 1, 0), value);
}


