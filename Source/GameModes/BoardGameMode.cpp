#include "BoardGameMode.h"
#include "HelheimGameInstance.h"
#include "Kismet/GameplayStatics.h"

TArray<FName> ABoardGameMode::randomMinigamesBag;

void ABoardGameMode::BeginPlay()
{
	UHelheimGameInstance* gi = GetGameInstance<UHelheimGameInstance>();

	UHelheimBoard* board = gi->GetBoard();
	bool boardSettingUp = false;
	
	if(!board) //Board doesn't exist for BoardGameMode, this means we are starting up. 
	{
		board = gi->SetupBoard();
		boardSettingUp = true;
	}

	board->OnGamemodeBegin();

	Super::BeginPlay();

	if (boardSettingUp)
	{
		OnPostBoardSetup();
	}
}

void ABoardGameMode::SetupTiles(AHelheimBoardTile* firstTile)
{
	const ESetupTilesResult result = SetupTilesRecursive(firstTile);

	switch(result)
	{
		case ESetupTilesResult::SetupLoop:
			//Handle loop.
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Cyan, FString::Printf(TEXT("Loop - Total Elements: %i"), boardTiles.Num()));
			break;
		case ESetupTilesResult::SetupPointToPoint:
			//Handle point to point.
			GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Cyan, FString::Printf(TEXT("P2P - Total Elements: %i"), boardTiles.Num()));
			break;
		case ESetupTilesResult::RecursiveLoop:
			checkf(false, TEXT("Recursive Loop tile setup."));
			break;
		case ESetupTilesResult::Invalid:
			checkf(false, TEXT("Invalid tile setup."));
			break;
	}
}

ESetupTilesResult ABoardGameMode::SetupTilesRecursive(AHelheimBoardTile* tile)
{
	//KS - This is the end of the line son.
	if(boardTiles.Num() > 0)
	{
		int foundIndex = boardTiles.Find(tile);
		if (foundIndex != INDEX_NONE)
		{
			return foundIndex ? ESetupTilesResult::RecursiveLoop : ESetupTilesResult::SetupLoop;
		}
	}

	if (!tile) 
	{
		return ESetupTilesResult::Invalid;
	}

	ESetupTilesResult result = ESetupTilesResult::SetupPointToPoint;

	boardTiles.Add(tile);
	tile->OnTileAdded();
	for (int i = tile->branches.Num()-1; i >= 0; i--)
	{
		if (!tile->branches[i].isReconnecting)
		{
			result = SetupTilesRecursive(tile->branches[i].tile);
		}
	}

	return result;
}

void ABoardGameMode::StartRandomMinigame_Implementation()
{
	const UHelheimGameInstance* gi = GetGameInstance<UHelheimGameInstance>();
	if (!randomMinigamesBag.Num())
	{
		randomMinigamesBag = gi->availableMinigameMaps;
	}

	int index = FMath::RandRange(0, randomMinigamesBag.Num() - 1);
	FName minigameName = randomMinigamesBag[index];
	randomMinigamesBag.RemoveAt(index);

	if(!randomMinigamesBag.Num())
	{
		randomMinigamesBag = gi->availableMinigameMaps;
		randomMinigamesBag.Remove(minigameName);
	}

	UGameplayStatics::OpenLevel(this, minigameName);
}

void ABoardGameMode::StartMinigame(int index)
{
	const UHelheimGameInstance* gi = GetGameInstance<UHelheimGameInstance>();
	UGameplayStatics::OpenLevel(this, gi->availableMinigameMaps[index]);
}

void ABoardGameMode::MovePremiumCurrencyMerchantTile()
{
	UHelheimBoard* board = GetGameInstance<UHelheimGameInstance>()->GetBoard();
	board->ChooseRandomPremiumCurrencyMerchantTile();
}

int ABoardGameMode::GetTotalRounds() 
{ 
	return UHelheimBoard::GetMaxRounds(); 
}

int ABoardGameMode::GetCurrentRound()
{
	UHelheimBoard* board = GetGameInstance<UHelheimGameInstance>()->GetBoard();
	return board->GetCurrentRound();
}