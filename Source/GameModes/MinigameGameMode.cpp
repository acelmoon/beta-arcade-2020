#include "MinigameGameMode.h"
#include "HelheimGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Widgets/HelheimMinigameInformWidget.h"
#include "Widgets/MinigameResultsWidget.h"

AMinigameGameMode::AMinigameGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	static ConstructorHelpers::FClassFinder<UUserWidget> MinigameInformClass(TEXT("WidgetBlueprint'/Game/Widgets/MinigameControls/UMG_MinigameInform.UMG_MinigameInform_C'"));
	informWidgetClass = MinigameInformClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> ResultsClass(TEXT("WidgetBlueprint'/Game/Widgets/MinigameResults/UMG_MinigameResultsWidgets.UMG_MinigameResultsWidgets_C'"));
	resultsWidgetClass = ResultsClass.Class;
}

void AMinigameGameMode::StartPlay()
{
	Super::StartPlay();

	UHelheimMinigameInformWidget* widget = CreateWidget<UHelheimMinigameInformWidget>(GetWorld(), informWidgetClass);
	widget->AddToViewport();

	StartMinigame();
}

void AMinigameGameMode::StartMinigame()
{

}

void AMinigameGameMode::EndMinigame()
{
	if (showRewardScreen)
	{
		if (!endGameTimerHandle.IsValid())
		{
			FTimerDelegate showUIDelegate = FTimerDelegate::CreateLambda([&]()
				{
					UMinigameResultsWidget* widget = CreateWidget<UMinigameResultsWidget>(GetWorld(), resultsWidgetClass);
					widget->AddToViewport();

					widget->OnReadyUp.AddDynamic(this, &AMinigameGameMode::LevelTransitionBack);
				});

			GetWorldTimerManager().SetTimer(endGameTimerHandle, showUIDelegate, 1.0f, false);
		}
	}
	else 
	{
		LevelTransitionBack();
	}
}

void AMinigameGameMode::LevelTransitionBack()
{
	if (UHelheimBoard* board = GetGameInstance<UHelheimGameInstance>()->GetBoard())
	{
		board->SetBoardState(EBoardState::TransitionFromMinigame);
		UGameplayStatics::OpenLevel(this, TEXT("Board"));
	}
	else
	{
		UGameplayStatics::OpenLevel(this, TEXT("Menu"));
	}
}

int AMinigameGameMode::GetPlayerEndPlace_Implementation(APlayerController* playerController)
{
	return 0;
}