#pragma once

#include "CoreMinimal.h"
#include "HelheimGameModeBase.h"
#include "Actors/Board/HelheimBoardTile.h"
#include "BoardGameMode.generated.h"

UENUM()
enum ESetupTilesResult
{
	SetupLoop,
	SetupPointToPoint,
	RecursiveLoop,
	Invalid
};

UCLASS()
class BETAARCADE_API ABoardGameMode : public AHelheimGameModeBase
{
	GENERATED_BODY()

protected:
	void BeginPlay() override;

public:
	TArray<class APremiumCurrencyMerchantTile*> premiumCurrencyMerchantTiles;

protected:
	static TArray<FName> randomMinigamesBag;

	UPROPERTY(BlueprintReadOnly)
	TArray<AHelheimBoardTile*> boardTiles;

public:
	void SetupTiles(AHelheimBoardTile* firstTile);

	UFUNCTION(BlueprintImplementableEvent)
	void OpenBranchSelector(APlayerController* controller);

	UFUNCTION(BlueprintImplementableEvent)
	void OnPostBoardSetup();

	UFUNCTION(BlueprintNativeEvent)
	void StartRandomMinigame();
	void StartMinigame(int index);

	AHelheimBoardTile* GetTile(int index) const { return boardTiles[index]; };
	int GetTileIndex(AHelheimBoardTile* tile) const { return boardTiles.Find(tile); };

	UFUNCTION(BlueprintCallable)
	void MovePremiumCurrencyMerchantTile();

private:
	ESetupTilesResult SetupTilesRecursive(AHelheimBoardTile* tile);

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FString GetCurrencyName() { return "Soul"; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FString GetPremiumCurrencyName() { return "Medallion"; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static int GetPremiumCurrencyPrice() { return 100; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static int GetTotalRounds();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentRound();

};
