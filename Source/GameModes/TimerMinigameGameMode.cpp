#include "TimerMinigameGameMode.h"

ATimerMinigameGameMode::ATimerMinigameGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 0.5f;
}

void ATimerMinigameGameMode::StartMinigame()
{
	Super::StartMinigame();
	startTime = GetWorld()->GetTimeSeconds();
}

void ATimerMinigameGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	const float elapsedTime = GetWorld()->GetTimeSeconds() - startTime;
	const float timeLeft = timerDuration - elapsedTime;
	
	if(timeLeft < 0.0f)
	{
		EndMinigame();
	}

	UpdateTimerUI(timeLeft);
}