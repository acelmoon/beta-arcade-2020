#include "MenuGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Utilities/Typedefs.h"
#include "PlayerControllers/MenuPlayerController.h"
#include "HelheimLocalPlayer.h"
#include "HelheimGameInstance.h"
#include "HelheimBoard.h"

void AMenuGameMode::BeginPlay()
{
	for (ControllerId controllerId = 0; controllerId < 4; controllerId++) {
		APlayerController* playerController = UGameplayStatics::CreatePlayer(this, controllerId);
	}

	Super::BeginPlay();
}

void AMenuGameMode::StartGame(int maxRounds, int maxPremiumCurrency)
{
	GetGameInstance<UHelheimGameInstance>()->ResetBoard();
	UHelheimBoard::SetEndConditions(maxRounds, maxPremiumCurrency);
	StartLevel(TEXT("Board"));
}

void AMenuGameMode::StartLevel(FName minigameName)
{
	TArray<const AMenuPlayerController*> UnreadiedPlayers;

	for (auto PlayerIt = GetWorld()->GetPlayerControllerIterator(); PlayerIt; ++PlayerIt)
	{
		const AMenuPlayerController* pc = Cast<AMenuPlayerController>(PlayerIt->Get());
		if (!pc->IsReady())
		{
			UnreadiedPlayers.Add(pc);
		}
	}

	//KS - 1 player shouldn't be able to play with themselves. Lmao
	if (UnreadiedPlayers.Num() > 2) return;

	UHelheimGameInstance* gi = GetGameInstance<UHelheimGameInstance>();

	for (auto playerToRemove : UnreadiedPlayers)
	{
		gi->RemoveLocalPlayer(playerToRemove->GetLocalPlayer());
	}

	UGameplayStatics::OpenLevel(this, minigameName);
}