#pragma once

#include "CoreMinimal.h"
#include "GameModes/MinigameGameMode.h"
#include "LivesMinigameGameMode.generated.h"

class APlayerController;

UCLASS()
class BETAARCADE_API ALivesMinigameGameMode : public AMinigameGameMode
{
	GENERATED_BODY()

public:
	void OnCharacterDeath(APlayerController* playerController);

protected:
	virtual int GetPlayerEndPlace_Implementation(APlayerController* playerController) override;

private:
	TMap<APlayerController*, float> deadPlayers;

};
