#pragma once

#include "CoreMinimal.h"
#include "HelheimGameModeBase.h"
#include "MenuGameMode.generated.h"

UCLASS()
class BETAARCADE_API AMenuGameMode : public AHelheimGameModeBase
{
	GENERATED_BODY()

public:
	void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void StartGame(int maxRounds, int maxPremiumCurrency);

	UFUNCTION(BlueprintCallable)
	void StartLevel(FName minigameName);
};
