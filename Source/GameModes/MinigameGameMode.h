#pragma once

#include "CoreMinimal.h"
#include "HelheimGameModeBase.h"
#include "HelheimPowerUp.h"
#include "HelheimPickUp.h"
#include "MinigameGameMode.generated.h"

UCLASS()
class BETAARCADE_API AMinigameGameMode : public AHelheimGameModeBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	int GetPlayerEndPlace(APlayerController* playerController);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<TSubclassOf<AHelheimPowerUp>> PowerUps;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<TSubclassOf<AHelheimPickUp>> PickUps;

	UPROPERTY(EditDefaultsOnly)
	bool showRewardScreen = true;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<AHelheimPowerUp> GetRandomPowerUpClass() const { return PowerUps[FMath::RandRange(0, PowerUps.Num() - 1)]; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<AHelheimPickUp> GetRandomPickUpClass() const { return PickUps[FMath::RandRange(0, PickUps.Num() - 1)]; };

protected:
	virtual void EndMinigame();
	virtual void StartMinigame();

	virtual int GetPlayerEndPlace_Implementation(APlayerController* playerController);

	void StartPlay() override;

	UFUNCTION()
	void LevelTransitionBack();


	AMinigameGameMode(const FObjectInitializer& ObjectInitializer);

private:
	TSubclassOf<class UHelheimMinigameInformWidget> informWidgetClass;
	TSubclassOf<class UMinigameResultsWidget> resultsWidgetClass;

	FTimerHandle endGameTimerHandle;
};
