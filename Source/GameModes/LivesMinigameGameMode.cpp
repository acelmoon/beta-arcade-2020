#include "LivesMinigameGameMode.h"
#include "FunctionLibraries/HelheimBlueprintFunctionLibrary.h"
#include "GameFramework/PlayerController.h"

void ALivesMinigameGameMode::OnCharacterDeath(APlayerController* playerController)
{
	deadPlayers.Add(playerController, GetWorld()->GetTimeSeconds());
	const int totalAlive = GetWorld()->GetNumPlayerControllers() - deadPlayers.Num();

	if(totalAlive < 2)
	{
		EndMinigame();
	}
}

int ALivesMinigameGameMode::GetPlayerEndPlace_Implementation(APlayerController* playerController)
{
	int endPlace = 0;
	if(const float* deathTime = deadPlayers.Find(playerController))
	{
		endPlace++;

		for(TPair<APlayerController*, float> deadPlayer : deadPlayers)
		{
			if(deadPlayer.Key != playerController)
			{
				endPlace += (FMath::IsNearlyEqual(*deathTime, deadPlayer.Value, 0.5f) || *deathTime < deadPlayer.Value);
			}
		}
	}

	return FMath::Min(GetWorld()->GetNumPlayerControllers() - 1, endPlace);
}