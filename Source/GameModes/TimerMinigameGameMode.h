#pragma once

#include "CoreMinimal.h"
#include "GameModes/MinigameGameMode.h"
#include "TimerMinigameGameMode.generated.h"

UCLASS()
class BETAARCADE_API ATimerMinigameGameMode : public AMinigameGameMode
{
	GENERATED_UCLASS_BODY()
	
protected:
	virtual void StartMinigame() override;

	void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateTimerUI(float TimeLeft);

protected:
	UPROPERTY(EditDefaultsOnly)
	float timerDuration = 30.0f;

	float startTime = -1.f;
};
