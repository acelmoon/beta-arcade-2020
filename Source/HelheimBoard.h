#pragma once

#include "CoreMinimal.h"
#include "Utilities/Typedefs.h"
#include "HelheimBoard.generated.h"


UENUM()
enum EBoardState
{
	Uninitialised,
	OnBoard,
	TransitionToMinigame,
	TransitionFromMinigame,
};

USTRUCT()
struct FPlayerBoardData
{
public:
	GENERATED_USTRUCT_BODY()

	int tileIndex = 0;
	int soulsCount = 0;
	int premiumCurrencyCount = 0;
	int endPlaceInMinigame = 0;
};

class BETAARCADE_API UHelheimBoard
{
	TMap<ControllerId, FPlayerBoardData> playersBoardData;

public:
	UHelheimBoard(class UHelheimGameInstance* gi);
	const FPlayerBoardData* GetPlayerBoardData(ControllerId controllerId) const;
	int GetPositionCurrencyGainedViaMinigame(int position) const;
	int GetControllerIdLeaderboardPosition(ControllerId controllerId);
	int GetPlayerSoulsCount(ControllerId controllerId) const;
	int GetPlayerPremiumCurrencyCount(ControllerId controllerId) const;
	void AddPlayerSouls(ControllerId controllerId, int amount);
	void AddPlayerPremiumCurrency(ControllerId controllerId, int amount);
	void SetPlayerEndPlaceInMinigame(ControllerId controllerId, int place);

	class AHelheimPlayerController* GetCurrentPlayer();
	void SetCurrentPlayer(APlayerController* player);
	void SetCurrentPlayer(ControllerId player);

	void SwitchToNextPlayer();

	int GetPremiumCurrencyMerchantTileIndex() const { return premiumCurrencyMerchantTileIndex; };
	void ChooseRandomPremiumCurrencyMerchantTile();

	void OnGamemodeBegin();

	void SetBoardState(EBoardState state);

	static void SetEndConditions(int newMaxRounds, int newMaxPremiumCurrency) { maxRounds = newMaxRounds; newMaxPremiumCurrency; };
	static int GetMaxRounds() { return maxRounds; };
	int GetCurrentRound() { return currentRound; };

	class UHelheimGameInstance* gameInstance;

private:
	void TeleportCharactersToCachedPosition();
	void CacheCurrentPlayersBoardData();
	void CacheAllPlayersMinigameData();
	void ApplyMinigameRewards();
	void UpdatePremiumCurrencyMerchantTile();
	void HandleStatePreTransition();
	void HandleStatePostTransition();
	void TriggerResultScreen();

private:
	ControllerId currentPlayer = -1;
	EBoardState boardState = EBoardState::Uninitialised;

	const int soulsBaseRewardRate = 60;

	int premiumCurrencyMerchantTileIndex = -1;

	int currentRound = 1;

	static int maxRounds;
	static int maxPremiumCurrency;
};