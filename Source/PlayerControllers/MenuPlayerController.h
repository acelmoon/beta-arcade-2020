#pragma once

#include "CoreMinimal.h"
#include "HelheimPlayerController.h"
#include "MenuPlayerController.generated.h"

UCLASS()
class BETAARCADE_API AMenuPlayerController : public AHelheimPlayerController
{
	GENERATED_BODY()
	
	bool isReady = false;

public:
	UFUNCTION(BlueprintCallable)
	bool ToggleReady() { return isReady = !isReady; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsReady() const { return isReady; };

};
