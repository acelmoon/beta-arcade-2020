#include "BoardPlayerController.h"
#include "HelheimGameInstance.h"
#include "Widgets/HelheimPlayerUserWidget.h"
#include "Pawns/BoardPawn.h"

ABoardPlayerController::ABoardPlayerController() : Super()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> MarkerWidget(TEXT("/Game/Widgets/UMG_PlayerMarker"));
	playerMarkerWidgetClass = MarkerWidget.Class;
}

void ABoardPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	UHelheimPlayerUserWidget* playerMarkerWidget = CreateWidget<UHelheimPlayerUserWidget>(this, playerMarkerWidgetClass);
	playerMarkerWidget->SetupPlayerWidget(this);
	playerMarkerWidget->AddToViewport();

	if (GetGameInstance<UHelheimGameInstance>()->GetBoard()->GetCurrentPlayer() != this)
	{
		SetIsCurrentPlayer(false);
	}
}

void ABoardPlayerController::SetIsCurrentPlayer_Implementation(bool isCurrent)
{
	if(isCurrent)
	{
		EnableInput(nullptr);
	}
	else
	{
		DisableInput(nullptr);
	}

	Cast<ABoardPawn>(GetPawn())->SetIsCurrentPlayer(isCurrent);
}