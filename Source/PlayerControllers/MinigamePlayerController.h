#pragma once

#include "CoreMinimal.h"
#include "HelheimPlayerController.h"
#include "MinigamePlayerController.generated.h"

UCLASS()
class BETAARCADE_API AMinigamePlayerController : public AHelheimPlayerController
{
	GENERATED_BODY()
	
public:
	AMinigamePlayerController();

protected:
	void BeginPlay() override;

private:
	TSubclassOf<UUserWidget> playerMarkerWidgetClass;
};
