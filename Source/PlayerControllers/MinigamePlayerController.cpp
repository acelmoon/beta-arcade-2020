#include "MinigamePlayerController.h"
#include "Widgets/HelheimPlayerUserWidget.h"

AMinigamePlayerController::AMinigamePlayerController() : Super()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> MarkerWidget(TEXT("/Game/Widgets/UMG_PlayerMarker"));
	playerMarkerWidgetClass = MarkerWidget.Class;
}

void AMinigamePlayerController::BeginPlay()
{
	Super::BeginPlay();
	UHelheimPlayerUserWidget* playerMarkerWidget = CreateWidget<UHelheimPlayerUserWidget>(this, playerMarkerWidgetClass);
	playerMarkerWidget->SetupPlayerWidget(this);
	playerMarkerWidget->AddToViewport();
} 