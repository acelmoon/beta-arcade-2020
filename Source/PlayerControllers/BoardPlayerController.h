#pragma once

#include "CoreMinimal.h"
#include "HelheimPlayerController.h"
#include "Actors/Board/HelheimBoardTile.h"
#include "BoardPlayerController.generated.h"

UCLASS()
class BETAARCADE_API ABoardPlayerController : public AHelheimPlayerController
{
	GENERATED_BODY()

	void BeginPlay() override;

public:
	ABoardPlayerController();

	UFUNCTION(BlueprintNativeEvent)
	void SetIsCurrentPlayer(bool isCurrent);

private:
	TSubclassOf<UUserWidget> playerMarkerWidgetClass;
};
