

#pragma once

#include "CoreMinimal.h"
#include "Engine/LocalPlayer.h"
#include "HelheimLocalPlayer.generated.h"

UCLASS()
class BETAARCADE_API UHelheimLocalPlayer : public ULocalPlayer
{
	GENERATED_BODY()

public:
	FLinearColor GetPlayerPrimaryColour() const;
	FLinearColor GetPlayerSecondaryColour() const;
};