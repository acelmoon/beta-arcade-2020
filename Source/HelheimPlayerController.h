#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "HelheimPlayerController.generated.h"

UCLASS(BlueprintType)
class BETAARCADE_API AHelheimPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AHelheimPlayerController();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FLinearColor GetPlayerColour() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FLinearColor GetPlayerSecondaryColour() const;
};