#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HelheimGameModeBase.generated.h"

UCLASS()
class BETAARCADE_API AHelheimGameModeBase : public AGameModeBase
{
	GENERATED_UCLASS_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FName GetRandomLevelStreamingName() const;
};
