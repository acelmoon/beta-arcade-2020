@echo off
git config lfs.setlockablereadonly false
echo Done! Please refer to the Setting up Source Control Presentation for further steps. Don't forget to set Editor to compile on startup!
pause